package com.runo.assignmentruno;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.runo.assignmentruno.helpers.AppConstants;
import com.runo.assignmentruno.helpers.PrabhaSharedPref;
import com.runo.assignmentruno.model.NotesDto;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static com.runo.assignmentruno.helpers.AppConstants.DOCUMENT_ID;
import static com.runo.assignmentruno.helpers.AppConstants.KEY_ONE_TIME_LOGIN_MOBILE;
import static com.runo.assignmentruno.helpers.AppConstants.NOTE_DOC_ID;

public class ContentActivity extends AppCompatActivity
{
    Button addButtonBTID,retrieveButtonBTID,logoutButtonBTID;
    private String doc_Id,Note_docid;
    ListView list_LVID;
    EditText addtitile,addDescription;
    String s_title,s_description,Note_document_id;
    private FirebaseFirestore note_db = FirebaseFirestore.getInstance();
    //a list to store all the products
    private List<NotesDto> notes_disp = new ArrayList<>();
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null)
            {
                doc_Id = bundle.getString("Doc_id");
               // Note_docid=bundle.getString("Note_doc_Id");
                Toast.makeText(ContentActivity.this,doc_Id,Toast.LENGTH_LONG).show();
            }
        }
        list_LVID=findViewById(R.id.list_LVID);
        retrieveButtonBTID=findViewById(R.id.retrieveButtonBTID);
        addtitile=findViewById(R.id.title_ETID);
        addDescription=findViewById(R.id.description_ETID);
        addButtonBTID=findViewById(R.id.addnoteBTID);
        logoutButtonBTID=findViewById(R.id.logoutButtonBTID);
        logoutButtonBTID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
//                PrabhaSharedPref.setPreference(ContentActivity.this, KEY_ONE_TIME_LOGIN_MOBILE,"");
//                PrabhaSharedPref.setPreference(ContentActivity.this,DOCUMENT_ID,"");
//                PrabhaSharedPref.setPreference(ContentActivity.this,NOTE_DOC_ID,"");

                Intent logout_int=new Intent(ContentActivity.this,LoginActivity.class);
                startActivity(logout_int);
                finish();

            }
        });
        addButtonBTID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(addtitile.getText()!=null)
                    s_title = addtitile.getText().toString();
                if(addDescription.getText()!=null)
                    s_description = addDescription.getText().toString();
                if(s_title==null||s_title.isEmpty())
                {
                    Toast.makeText(ContentActivity.this, "Please enter your Title!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(s_description==null||s_description.isEmpty())
                {
                    Toast.makeText(ContentActivity.this, "Please enter your Description!", Toast.LENGTH_SHORT).show();
                    return;
                }
               // String Doc_id= PrabhaSharedPref.getPreferences(ContentActivity.this, DOCUMENT_ID);
                final NotesDto notesDto = new NotesDto( s_title, s_description);
                note_db.collection("Users").document(doc_Id).collection("Notes").add(notesDto).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference)
                    {
                        Note_document_id=documentReference.getId();
                        PrabhaSharedPref.setPreference(ContentActivity.this, NOTE_DOC_ID,Note_document_id);
                        Toast.makeText(ContentActivity.this,"Your Note is Saved!"+Note_document_id,Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ContentActivity.this,"Your Note is not Saved!",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        retrieveButtonBTID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               // String d_id=PrabhaSharedPref.getPreferences(ContentActivity.this, DOCUMENT_ID);
                note_db.collection("Users").document(doc_Id).collection("Notes").addSnapshotListener(new EventListener<QuerySnapshot>()
                {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        try {
                           // notes_disp.clear();
                            notes_disp=documentSnapshot.toObjects(NotesDto.class);
                            for (int i = 0; i < notes_disp.size(); i++) {
                                NotesDto dataModel = new NotesDto();
                                dataModel.setNote_title(notes_disp.get(i).getNote_title());
                                dataModel.setNote_Description(notes_disp.get(i).getNote_Description());
                                notes_disp.set(i, dataModel);
                            }
                            list_LVID.setAdapter(new MyAdapter(getApplicationContext(),R.layout.custom_dialog,notes_disp));

                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public class MyAdapter extends ArrayAdapter<NotesDto>
    {
        private Context context;
        private int resid;

        public MyAdapter(Context context,int resid,List<NotesDto> items)
        {
            super(context,resid,items);
            this.context=context;
            this.resid=resid;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v=convertView;


            if(v==null)
            {
                LayoutInflater layoutInflater;
                layoutInflater=LayoutInflater.from(context);
                v=layoutInflater.inflate(resid,null);
            }

            final NotesDto dataModelobj=getItem(position);

            if(dataModelobj !=null)
            {
                TextView t1=(TextView)v.findViewById(R.id.note_title);
                TextView t2=(TextView)v.findViewById(R.id.note_description);
                if(t1!=null)
                {
                    t1.setText(dataModelobj.getNote_title());
                }
                if(t2!=null)
                {
                    t2.setText(dataModelobj.getNote_Description());
                }

            }
            return v;
        }
    }

}
