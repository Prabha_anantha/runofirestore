package com.runo.assignmentruno.model;

import java.io.Serializable;

public class RegDTO implements Serializable
{
    private String doc_id;
    private String name;
    private String phone;
    private String password;

    public RegDTO() {
    }

    public RegDTO(String doc_id, String name, String phone, String password) {
        this.doc_id = doc_id;
        this.name = name;
        this.phone = phone;
        this.password = password;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
