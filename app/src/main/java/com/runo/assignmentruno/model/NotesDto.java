package com.runo.assignmentruno.model;

public class NotesDto
{
    private String note_title;
    private String note_Description;

    public NotesDto() {
    }

    public NotesDto(String note_title, String note_Description) {
        this.note_title = note_title;
        this.note_Description = note_Description;
    }

    public String getNote_title() {
        return note_title;
    }

    public void setNote_title(String note_title) {
        this.note_title = note_title;
    }

    public String getNote_Description() {
        return note_Description;
    }

    public void setNote_Description(String note_Description) {
        this.note_Description = note_Description;
    }

}
