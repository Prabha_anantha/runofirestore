package com.runo.assignmentruno;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.runo.assignmentruno.helpers.AppConstants;
import com.runo.assignmentruno.helpers.PrabhaSharedPref;

public class SplashActivity extends AppCompatActivity {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler=new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
//                Intent intent=new Intent(SplashActivity.this, LoginActivity.class);
//                startActivity(intent);
//                finish();

            String phone= PrabhaSharedPref.getPreferences(SplashActivity.this, AppConstants.KEY_ONE_TIME_LOGIN_MOBILE);
                if(phone.isEmpty())
                {
                    Log.d("RUNOActivity", "Status Code = " + phone);
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                else {
                    Intent i = new Intent(SplashActivity.this, ContentActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }
            }
        },3000);
    }
}
