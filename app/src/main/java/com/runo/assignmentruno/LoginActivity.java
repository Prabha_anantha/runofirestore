package com.runo.assignmentruno;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.runo.assignmentruno.helpers.AppConstants;
import com.runo.assignmentruno.helpers.PrabhaSharedPref;
import com.runo.assignmentruno.model.RegDTO;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static com.runo.assignmentruno.helpers.AppConstants.DOCUMENT_ID;
import static com.runo.assignmentruno.helpers.AppConstants.KEY_ONE_TIME_LOGIN_MOBILE;
import static com.runo.assignmentruno.helpers.AppConstants.NOTE_DOC_ID;

public class LoginActivity extends AppCompatActivity {

    EditText log_phone,log_password;
    Button login,signup;
    String Log_Phone,Log_Password;
    private FirebaseFirestore LogDb = FirebaseFirestore.getInstance();
    private List<RegDTO> regDTOS = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        PrabhaSharedPref.setPreference(LoginActivity.this, KEY_ONE_TIME_LOGIN_MOBILE,"");
//        PrabhaSharedPref.setPreference(LoginActivity.this,DOCUMENT_ID,"");
//        PrabhaSharedPref.setPreference(LoginActivity.this,NOTE_DOC_ID,"");
        log_phone=findViewById(R.id.log_phone_ETID);
        log_password=findViewById(R.id.log_password_ETID);
        login=findViewById(R.id.logButtonBTID);
        signup=findViewById(R.id.log_signButtonBTID);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup_intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(signup_intent);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(log_phone.getText()!=null)
                    Log_Phone = log_phone.getText().toString();
                if(log_password.getText()!=null)
                    Log_Password = log_password.getText().toString();
                if(Log_Phone==null||Log_Phone.isEmpty())
                {
                    Toast.makeText(LoginActivity.this, "Please enter your phone number!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Log_Password==null||Log_Password.isEmpty())
                {
                    Toast.makeText(LoginActivity.this, "Please enter your password!", Toast.LENGTH_SHORT).show();
                    return;
                }
                PrabhaSharedPref.setPreference(LoginActivity.this, KEY_ONE_TIME_LOGIN_MOBILE,Log_Phone);
               CollectionReference collectionRef = LogDb.collection("Users");
                Query query = collectionRef.whereEqualTo("phone", Log_Phone );
                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful())
                        {
                            QuerySnapshot qSnap = task.getResult();
                            if (!qSnap.isEmpty())
                            {
                                Toast.makeText(LoginActivity.this, Log_Phone, Toast.LENGTH_LONG).show();
                            Intent log_int = new Intent(LoginActivity.this, ContentActivity.class);
                            String unique_id=PrabhaSharedPref.getPreferences(LoginActivity.this, DOCUMENT_ID);
                            Toast.makeText(LoginActivity.this,unique_id,Toast.LENGTH_LONG).show();
                            log_int.putExtra("Doc_id",unique_id);
                            startActivity(log_int);
                            finish();

                            } else {
                                Toast.makeText(LoginActivity.this, "There is no data!", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }
                });

            }
        });
    }
}
