package com.runo.assignmentruno;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button signin,signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signin=findViewById(R.id.signinButtonBTID);
        signup=findViewById(R.id.signupButtonBTID);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signup_intent=new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(signup_intent);
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signin_intent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(signin_intent);
            }
        });
    }
}
