package com.runo.assignmentruno;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.runo.assignmentruno.helpers.AppConstants;
import com.runo.assignmentruno.helpers.PrabhaSharedPref;
import com.runo.assignmentruno.model.RegDTO;

import static com.runo.assignmentruno.helpers.AppConstants.DOCUMENT_ID;

public class RegisterActivity extends AppCompatActivity
{

    EditText name,phone,password;
    Button save;
    String Name_Reg,Phone_Reg,Password_Reg,document_ID;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        name=findViewById(R.id.reg_name_ETID);
        phone=findViewById(R.id.reg_phone_ETID);
        password=findViewById(R.id.reg_password_ETID);
        save=findViewById(R.id.saveButtonBTID);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.getText()!=null)
                    Name_Reg = name.getText().toString();
                if(phone.getText()!=null)
                    Phone_Reg = phone.getText().toString();
                if(password.getText()!=null)
                    Password_Reg = password.getText().toString();

                if(Name_Reg==null||Name_Reg.isEmpty())
                {
                    Toast.makeText(RegisterActivity.this, "Please enter your name!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Phone_Reg==null||Phone_Reg.isEmpty())
                {
                    Toast.makeText(RegisterActivity.this, "Please enter your phone number!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Password_Reg==null||Password_Reg.isEmpty())
                {
                    Toast.makeText(RegisterActivity.this, "Please enter your password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                 final RegDTO regDTO = new RegDTO( "",Name_Reg, Phone_Reg, Password_Reg);
                db.collection("Users").add(regDTO)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>()
                        {
                            @Override
                            public void onSuccess(DocumentReference documentReference)
                            {
                                PrabhaSharedPref.setPreference(RegisterActivity.this, AppConstants.KEY_ONE_TIME_LOGIN_MOBILE,regDTO.getPhone());
                                Intent reg_int = new Intent(RegisterActivity.this, ContentActivity.class);
                                document_ID=documentReference.getId();
                               PrabhaSharedPref.setPreference(RegisterActivity.this,DOCUMENT_ID,document_ID);
                                reg_int.putExtra("Doc_id",document_ID);
                                startActivity(reg_int);
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener()
                        {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(), "record creation failed!", Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

    }
}
