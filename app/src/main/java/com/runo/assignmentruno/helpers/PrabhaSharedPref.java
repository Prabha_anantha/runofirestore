package com.runo.assignmentruno.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class PrabhaSharedPref
{
    // shared pref mode
    int PRIVATE_MODE = 0;
    SharedPreferences.Editor editorS;
    Context _context;
    SharedPreferences pref_S;

    public static void setPreference(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPreferences(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

}
